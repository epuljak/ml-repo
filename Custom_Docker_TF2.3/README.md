# Building custom images

Repository for building custom images to use for Kubeflow Notebook Servers, Pipelines and Katib.

## To build an image:
1) `git clone https://gitlab.cern.ch/ai-ml/custom_ml_images.git`
2) `cd custom_ml_images`
3) Edit _Dockerfile_ and/or _requirements.txt_
4) `docker build . -f Dockerfile -t gitlab-registry.cern.ch/ai-ml/CUSTOM_REPO_PATH`
5) `docker push gitlab-registry.cern.ch/ai-ml/CUSTOM_REPO_PATH`
